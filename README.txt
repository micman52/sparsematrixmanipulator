Creates 3 matrices from files, representing them as a lisked list of linked lists,
excluding cells containing zero, then performs addition on the first two
and transposition on the last.
