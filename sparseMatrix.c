/*
MacKinley  Trudeau
cis190
project 3 p2
12/4
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct COL
{
	//column number
	int column;

	//value
	int val;

	//pointer to next node
	struct COL *next;
}CNODE;

typedef struct ROW
{
	//row number
	int rowNum;

	//pointer to first column node
	CNODE *firstCol, *lastCol;

	//pointer to next row node
	struct ROW *next;
}RNODE;

typedef struct
{
	//reference to first row struct of the matrix
	RNODE *firstRow;

	//dimensions of the matrix
	int numCols, numRows;
}MATRIX;

//makes a matrix given a input file pointer
void makeMatrix(FILE *fp, MATRIX *mtrx);

//prints a matrix
void printMatrix(MATRIX mtrx);

//free memory
void freeMatrix(MATRIX *mtrx);

//sum 2 matrixs of same dimensions
void sum(MATRIX mtrx1, MATRIX mtrx2, MATRIX *mtrxProduct);

//returns a reference to the first row of a transposed matrix
void transpose(MATRIX mtrx, MATRIX *mtrxTransposed);

int main(void)
{http://www.amazon.com/b/ref=sv_dmusic_5?ie=UTF8&node=2258933011
	//orignal, transposed and product matrix structures are created
	MATRIX mtrx = {NULL, 0, 0}, mtrx2 = {NULL, 0, 0}, mtrx3 = {NULL, 0, 0}, mtrxTransposed = {NULL, 0, 0}, mtrxProduct = {NULL, 0, 0};
	FILE *fp;

	//open file
	fp = fopen("matrix.txt", "r+");

	//make first matrix
	makeMatrix(fp, &(mtrx));

	//close file
	fclose(fp);

	//open file
	fp = fopen("matrix2.txt", "r+");

	//make first matrix
	makeMatrix(fp, &(mtrx2));

	//close file
	fclose(fp);

	//open file
	fp = fopen("matrix3.txt", "r+");

	//make first matrix
	makeMatrix(fp, &(mtrx3));

	//close file
	fclose(fp);

	//create and set reference to transposed matrix
	transpose(mtrx3, &mtrxTransposed);

	//sum the first matrix with itself
	sum(mtrx, mtrx2, &mtrxProduct);

	//print first matrix
	printf("First matrix: \n\n");
	printMatrix(mtrx);

	printf("\n\t\t\t\t\t+\n\n");

	//print second matrix
	printf("Second matrix: \n\n");
	printMatrix(mtrx2);

	printf("\n\t\t\t\t\t=\n\n");

	//print product matrix
	printf("Product matrix: \n\n");
	printMatrix(mtrxProduct);

	//print third matrix
	printf("Third matrix: \n\n");
	printMatrix(mtrx3);

	printf("\n\t\t\t\t\tTranspose\n\n");

	//print transposed matrix
	printf("Third matrix transposed: \n\n");
	printMatrix(mtrxTransposed);

	/*free memory*/

	//free original matrix
	freeMatrix(&mtrx);
	//free transposed matrix
	freeMatrix(&mtrxTransposed);
	//free product matrix
	freeMatrix(&mtrxProduct);

	return 1;
}

void makeMatrix(FILE *fp, MATRIX *mtrx)
{

	//pointer to first and last row
	RNODE *pLastRow = NULL;

	//holds char read from file and num is the char converted to int
	char c;
	int num;
	//while there are still characters to read from file
	while((c = fgetc(fp)) != EOF)
	{
		//create a row struct
		if(mtrx -> firstRow == NULL)
		{
			//set first and last to the only row element
			mtrx -> firstRow = (RNODE *)malloc(sizeof(RNODE));
			pLastRow = mtrx -> firstRow;
			//init fields of first row
			mtrx -> firstRow -> rowNum = (mtrx -> numRows)++;
			mtrx -> firstRow -> firstCol = NULL;
			mtrx -> firstRow -> lastCol = NULL;
			mtrx -> firstRow -> next = NULL;
		}

		if(c == ' ')
		{
			continue;
		}else
		if(c == '\n')
		{
			//create now row node and have the old last one point at it
			pLastRow -> next = (RNODE *)malloc(sizeof(RNODE));
			//reassign the last row pointer to point at the new last row
			pLastRow = pLastRow -> next;

			//init its fields
			pLastRow -> rowNum = (mtrx -> numRows)++;
			pLastRow -> next = NULL;
			pLastRow -> firstCol = NULL;
			pLastRow -> lastCol = NULL;

			//reset number of columns for next row
			(mtrx -> numCols) = 0;

		}else
		{
			//convert character to int
			num = c - '0';

			//if value is not equal to zero we wil create a column node
			if(num != 0)
			{
				//if there are no columns in this row yet
				if(pLastRow -> firstCol == NULL)
				{
					//create a column
					pLastRow -> firstCol = (CNODE *)malloc(sizeof(CNODE));
					//set its fields
					pLastRow -> firstCol -> column = (mtrx -> numCols)++;
					pLastRow -> firstCol -> val = num;
					pLastRow -> firstCol -> next = NULL;
					//set the rows lastCol to also point at this column since it is the only one (first and last)
					pLastRow -> lastCol = pLastRow -> firstCol;
				}else
				{
					//create a new column and set the old last column to point to it
					pLastRow -> lastCol -> next = (CNODE *)malloc(sizeof(CNODE));

					//reassign the rows last column pointer to the new last column
					pLastRow -> lastCol = pLastRow -> lastCol -> next;
					//set its fields
					pLastRow -> lastCol -> column = (mtrx -> numCols)++;
					pLastRow -> lastCol -> val = num;
					pLastRow -> lastCol -> next = NULL; 
				}
			}else
			{
				//if number is zero do not create a column but increment the column counter
				(mtrx -> numCols)++;
			}
		}
	}

	return;
}

//sum 2 matrixs of same dimensions
void sum(MATRIX mtrx1, MATRIX mtrx2, MATRIX *mtrxProduct)
{
	if(mtrx1.numRows != mtrx2.numRows && mtrx1.numCols != mtrx2.numCols)
	{
		printf("Sum operation not performed,\nMatrixs do not have the same dimensions.\n");
		return;
	}

	RNODE *pRow1, *pRow2, *pLastProductRow;
	CNODE *pCol1, *pCol2, *pProductCol;

	//set rows and cols in product matrix
	mtrxProduct -> numRows = mtrx1.numRows;
	mtrxProduct -> numCols = mtrx1.numCols;

	pRow1 = mtrx1.firstRow;
	pRow2 = mtrx2.firstRow;

	while(pRow1 != NULL && pRow2 != NULL)
	{
		if(mtrxProduct -> firstRow == NULL)
		{
			mtrxProduct -> firstRow = (RNODE *)malloc(sizeof(RNODE));
			pLastProductRow = mtrxProduct -> firstRow;

			pLastProductRow -> rowNum = 0;
			pLastProductRow -> firstCol = NULL;
			pLastProductRow -> lastCol = NULL;
			pLastProductRow -> next = NULL;
		}
		else
		{
			pLastProductRow -> next = (RNODE *)malloc(sizeof(RNODE));

			pLastProductRow -> next -> rowNum = (pLastProductRow -> rowNum) + 1;
			pLastProductRow = pLastProductRow -> next;

			pLastProductRow -> firstCol = NULL;
			pLastProductRow -> lastCol = NULL;
			pLastProductRow -> next = NULL;
		}

		pCol1 = pRow1 -> firstCol;
		pCol2 = pRow2 -> firstCol;

		while(pCol1 != NULL && pCol2 != NULL)
		{
			if(pLastProductRow -> firstCol == NULL)
			{
				pLastProductRow -> firstCol = (CNODE *)malloc(sizeof(CNODE));
				pLastProductRow -> lastCol = pLastProductRow -> firstCol;
			}
			else
			{
				pLastProductRow -> lastCol -> next = (CNODE *)malloc(sizeof(CNODE));
				pLastProductRow -> lastCol = pLastProductRow -> lastCol -> next;
			}

			pProductCol = pLastProductRow -> lastCol;

			pProductCol -> next = NULL;

			//if they are on the same column
			if(pCol1 -> column == pCol2 -> column)
			{
				pProductCol -> column = pCol2 -> column;
				pProductCol -> val = pCol1 -> val + pCol2 -> val;

				pCol1 = pCol1 -> next;
				pCol2 = pCol2 -> next;
			}else
			//if first is earlier than second column
			if(pCol1 -> column < pCol2 -> column)
			{
				pProductCol -> column = pCol1 -> column;
				pProductCol -> val = pCol1 -> val;

				pCol1 = pCol1 -> next;
			}
			else
			//if first is later than second column
			if(pCol1 -> column > pCol2 -> column)
			{
				pProductCol -> column = pCol2 -> column;
				pProductCol -> val = pCol2 -> val;

				pCol2 = pCol2 -> next;
			}
		}

		/*One of the rows has more non-zero columns than the other
		  so find out which one has more and add the rest of those 
		  to the product matrix*/

		while(pCol1 != NULL)
		{
			if(pLastProductRow -> firstCol == NULL)
			{
				pLastProductRow -> firstCol = (CNODE *)malloc(sizeof(CNODE));
				pLastProductRow -> lastCol = pLastProductRow -> firstCol;
			}
			else
			{
				pLastProductRow -> lastCol -> next = (CNODE *)malloc(sizeof(CNODE));
				pLastProductRow -> lastCol = pLastProductRow -> lastCol -> next;
			}

			pProductCol = pLastProductRow -> lastCol;
			pProductCol -> next = NULL;

			//assign
			pProductCol -> column = pCol1 -> column;
			pProductCol -> val = pCol1 -> val;

			//get next col
			pCol1 = pCol1 -> next;
		}

		while(pCol2 != NULL)
		{
			if(pLastProductRow -> firstCol == NULL)
			{
				pLastProductRow -> firstCol = (CNODE *)malloc(sizeof(CNODE));
				pLastProductRow -> lastCol = pLastProductRow -> firstCol;
			}
			else
			{
				pLastProductRow -> lastCol -> next = (CNODE *)malloc(sizeof(CNODE));
				pLastProductRow -> lastCol = pLastProductRow -> lastCol -> next;
			}

			pProductCol = pLastProductRow -> lastCol;
			pProductCol -> next = NULL;

			//assign
			pProductCol -> column = pCol2 -> column;
			pProductCol -> val = pCol2 -> val;

			//get next col
			pCol2 = pCol2 -> next;
		}

		pRow1 = pRow1 -> next;
		pRow2 = pRow2 -> next;
	}

return;
}

//prints a matrix
void printMatrix(MATRIX mtrx)
{
	int num = 0, numCols = mtrx.numCols;

	CNODE *tempC = NULL;
	RNODE *tempR = mtrx.firstRow;

	printf("Column:");
	//print column heading
	for(num = 0; num < numCols; num++)
		printf("\t#%d", num);

	printf("\n");

	while(tempR != NULL)
	{
		printf("Row %d:\t", tempR -> rowNum);
		tempC = tempR -> firstCol;

		if(tempC != NULL)
			num = tempC -> column;
		else
		{
			for(num = 0; num < numCols; num++)
				printf("0\t");
		}

		while(tempC != NULL)
		{

			while(num > 0)
			{
				printf("0\t");
				num--;
			}

			printf("%d\t",tempC -> val);

			//get the number of zeros between the 2 columns
			if(tempC -> next != NULL)
				num = (tempC -> next -> column) - (tempC -> column) - 1;
			else if((num = numCols - tempC -> column - 1) <= numCols - 1)
				{
					while(num > 0)
					{
						printf("0\t");
						num--;
					}
				}

			tempC = tempC -> next;
		}

		printf("\n");
		tempR = tempR -> next;
	}
	printf("\n");

	return;
}

//free memory
void freeMatrix(MATRIX *mtrx)
{
	//temporary row node pointer
	RNODE *tempR = NULL;
	//temporary column node pointer
	CNODE *tempC = NULL;

	//get first row
	tempR = mtrx -> firstRow;
	
	//while there are still rows
	while(tempR != NULL)
	{

		//get first column from current row
		tempC = tempR -> firstCol;

		//while there are still columns in this row
		while(tempC != NULL)
		{	
			//set temp column pointer to next column
			tempC = tempC -> next;
			//free the previous column
			free(tempR -> firstCol);
			//set first column to the next column
			tempR -> firstCol = tempC;
		}

		/*now all column on this row have been freed*/

		//set temp row pointer to next row
		tempR = tempR -> next;
		//free the previous row
		free(mtrx -> firstRow);
		//set first row to the next row
		mtrx -> firstRow = tempR;
	}

	return;
}

//returns a reference to the first row of a transposed matrix
void transpose(MATRIX mtrx, MATRIX *mtrxTransposed)
{
	//new first and last row node pointers plus temp
	RNODE *newFirstRow = NULL, *newLastRow = NULL, *tempOrigRow, *tempNewRow;
	//temporary first and last column node pointer
	CNODE *tempOrigCol;

	int x, y, numRows = mtrx.numRows, numCols = mtrx.numCols;

	//if columns/matrix exist
	if(numCols != 0)
	{
		//create and get direct reference to first row
		newFirstRow = (RNODE *)malloc(sizeof(RNODE));

		//init fields of first row
		newFirstRow -> rowNum = 0;
		newFirstRow -> firstCol = NULL;
		newFirstRow -> lastCol = NULL;
		newFirstRow -> next = NULL;

		//set last row pointer to point to same one (since its the only one its first and last)
		newLastRow = newFirstRow;

		//make the rest of the row nodes
		for(x = 1; x < numCols; x++)
		{
			//create and link new row
			newLastRow -> next = (RNODE *)malloc(sizeof(RNODE));
			//set last to the new row
			newLastRow = newLastRow -> next;
			//init fields of the new row
			newLastRow -> rowNum = x;
			newLastRow -> firstCol = NULL;
			newLastRow -> lastCol = NULL;
			newLastRow -> next = NULL;
		}

		/*all row nodes created*/

	}

	/*create columns from original matrix*/

	//for each row
	for(x = 0; x < numRows; x++)
	{
		//assign temp row to orignal matrix
		tempOrigRow = mtrx.firstRow;

		//move temp row down x many rows
		for(y = 0; y < x; y++)
			tempOrigRow = tempOrigRow -> next;

		//get first column from current row
		tempOrigCol = tempOrigRow -> firstCol;

		//while there are still columns
		while(tempOrigCol != NULL)
		{
			//get reference to first row of new matrix
			tempNewRow = newFirstRow;

			//the column number will now be the row number
			//so move down rows column-number times
			for(y = 0; y < tempOrigCol -> column; y++)
				tempNewRow = tempNewRow -> next;

			/*add new column to the end of the new row*/

			//if there are no columns in this row yet
			if(tempNewRow -> firstCol == NULL)
			{
				//create one and set first and last column pointer to it
				tempNewRow -> firstCol = (CNODE *)malloc(sizeof(CNODE));
				tempNewRow -> lastCol = tempNewRow -> firstCol;
			}//if there are alreader columns in this row
			else
			{
				//create column and add it to the end
				tempNewRow -> lastCol -> next = (CNODE *)malloc(sizeof(CNODE));
				//move last column pointer to new column
				tempNewRow -> lastCol = tempNewRow -> lastCol -> next;
			}

			//set all its fields
			tempNewRow -> lastCol -> column = x;
			tempNewRow -> lastCol -> val = tempOrigCol -> val;
			tempNewRow -> lastCol -> next = NULL;

			//get next column
			tempOrigCol = tempOrigCol -> next;
		}
	}

	/*Now that its created in memory set the fields in transposed matrix*/

	mtrxTransposed -> firstRow = newFirstRow;
	mtrxTransposed -> numRows = numCols;
	mtrxTransposed -> numCols = numRows;
return;
}